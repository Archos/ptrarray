#include <stdio.h>

void
foo()
{
    puts("foo!");
}

void
bar()
{
    puts("bar!");
}

void
baz()
{
    puts("baz!");
}

void
boo()
{
    puts("boo!");
}

typedef void (func)(void);

int 
main() {

    func *arr[] = {
	foo,
	bar,
	foo,
	bar,
	baz,
	bar,
	boo,
	NULL
    }, **i = NULL;

    i = arr;
    while (*i) (*i++)();

    return 0;
}
